<?php
require_once dirname(__FILE__)."/../vk/VkBotListener.php";
require_once dirname(__FILE__)."/util/VkMessage.php";

class SearchListener implements VkBotListener {
    protected $curl;
	protected $config = [];

    function __construct($config) {
        $this->config = $config;

        $this->curl = curl_init();
        curl_setopt_array($this->curl, array(
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => false,
        ));
    }
	
    public function getEventType() {
		return 4; // new messages
	}
	
	protected function search($query) {
		$params = $this->config;
		$params['q'] = $query;
		$url = 'https://www.googleapis.com/customsearch/v1?'. http_build_query($params);
        curl_setopt_array($this->curl, [
            CURLOPT_URL => $url,
        ]);

        $response = json_decode(curl_exec($this->curl), true);
		if (isset($response["error"])) {
			return "гугол ругается";
		}
		if (!isset($response["items"]) || !count($response["items"])) {
			return "не нашел нихуя";
		}
		return str_replace("\n", '', $response["items"][0]["snippet"]) ."<br>Подробнее: ". $response["items"][0]["link"];
	}
	
    public function execute(VkApi $api, Array $args) {
		$message = new VkMessage($args);
		
        $outbox = ($message->flags & 2) === 2;
        //if ($outbox) return; // do not process outgoing messages
        if (preg_match('/#(ботинок)\b/ui', $message->text)) return false;

        $regexps = [
            "/\bбот[инок]{0,1}[,]{0,1} найди ([\w\s]+)\b/ui" => function ($args) use ($api, $message) {
                $api->sendMessage($message->from_id, $this->search($args[1]));
            }
        ];

		$stopPropagation = false;
        foreach ($regexps as $regexp => $func) {
            if (preg_match($regexp, $message->text, $args)) {
                $func($args);
				$stopPropagation = true;
                break;
            }
        }
		return $stopPropagation;
	}
}